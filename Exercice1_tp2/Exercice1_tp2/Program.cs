﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercice1_tp2
{
    public class Program
    {
        public static bool CommenceParUneMajuscule(string phrase)
        {
            char premier_carac;
            premier_carac = phrase[0];
            if (premier_carac >= 'A' && premier_carac <= 'Z')
            {
                Console.WriteLine("La phrase commence par une majuscule");
                return true;
            }
            else
            {
                Console.WriteLine("La phrase ne commence pas par une majuscule");
                return false;
            }
        }
        public static bool FiniParUnPoint(string phrase)
        {
            char dernier_carac;
            dernier_carac = phrase[phrase.Length - 1];
            if (dernier_carac == '.')
            {
                Console.WriteLine("La phrase se termine par un point");
                return true;
            }
            else
            {
                Console.WriteLine("La phrase ne se termine pas par un point");
                return false;
            }
        }
        static void Main(string[] args)
        {
            string ma_phrase;
            Console.WriteLine("Saisissez une phrase");
            ma_phrase = Console.ReadLine();
            Console.WriteLine("Vous avez saisie {0}", ma_phrase);
            CommenceParUneMajuscule(ma_phrase);
            FiniParUnPoint(ma_phrase);
            Console.ReadKey();
        }
    }
}
