﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercice1_tp2;
namespace Exercice1_tp2_test
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestMajuscule()
        {
            Assert.IsTrue(Program.CommenceParUneMajuscule("Les framboises sont perchees sur le tabouret de mon grand-pere."));
        }
        [TestMethod]
        public void TestPoint()
        {
            Assert.IsTrue(Program.FiniParUnPoint("Les framboises sont perchees sur le tabouret de mon grand-pere."));
        }
    }
}
